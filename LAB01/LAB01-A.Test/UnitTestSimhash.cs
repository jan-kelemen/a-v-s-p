﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LAB01_A.Test
{
    [TestClass]
    public class UnitTestSimhash
    {
        [TestMethod]
        public void TestSimhash()
        {
            var res = Simhash.Calculate("fakultet elektrotehnike i racunarstva");
            Assert.AreEqual("f27c6b49c8fcec47ebeef2de783eaf57", Simhash.ConvertToHex(res));
        }

        [TestMethod]
        public void TestHammingDistance()
        {
            Assert.AreEqual(8, Simhash.HammingDistance(new byte[] { 0xff }, new byte[] { 0x00 }));
            Assert.AreEqual(3, Simhash.HammingDistance(new byte[] { 0x03, 0x83 }, new byte[] { 0x83, 0x02 }));
        }
    }
}
