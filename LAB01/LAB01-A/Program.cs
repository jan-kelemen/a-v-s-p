﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAB01_A
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());

            var hashes = new List<byte[]>(n);
            for (var i = 0; i < n; i++)
            {
                hashes.Add(Simhash.Calculate(Console.ReadLine().Trim()));
            }

            var q = int.Parse(Console.ReadLine());
            for (var j = 0; j < q; j++)
            {
                var parts = Console.ReadLine().Split(' ');
                var i = int.Parse(parts[0]);
                var k = int.Parse(parts[1]);

                var result = ExecuteQuery(hashes, i, k);
                Console.WriteLine(result);
            }
        }

        public static int ExecuteQuery(List<byte[]> hashes, int i, int k)
        {
            var count = 0;
            var hash = hashes[i];
            for(var j = 0; j < hashes.Count; j++)
            {
                if(j != i)
                {
                    var distance = Simhash.HammingDistance(hashes[j], hash);
                    if(distance <= k)
                    {
                        count++;
                    }
                }
            }
            return count;
        }
    }
}
