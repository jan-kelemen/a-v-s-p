﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LAB01_B.Test
{
    [TestClass]
    public class MyTestClass
    {
        [TestMethod]
        public void TestLSHCandidates()
        {
            var b1 = new byte[] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06 };
            var b2 = new byte[] { 0x01, 0x02, 0x04, 0x05, 0x06, 0x07 };
            var b3 = new byte[] { 0xff, 0xff, 0x03, 0x04, 0x06, 0x07 };
            var b4 = new byte[] { 0xff, 0xff, 0x00, 0x00, 0x00, 0x00 };

            var candidates = LSH.CalculateCandidates(new System.Collections.Generic.List<byte[]>() { b1, b2, b3, b4 }, 3);
        }
    }
}
