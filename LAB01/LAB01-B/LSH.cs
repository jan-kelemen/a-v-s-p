﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAB01_B
{
    public class LSH
    {
        public static List<ISet<int>> CalculateCandidates(List<byte[]> hashes, int b)
        {
            var candidates = createCandidatesMap(hashes.Count);
            for (var band = 0; band < b; band++)
            {
                var buckets = new Dictionary<int, HashSet<int>>();
                for(var currentId = 0; currentId < hashes.Count; currentId++)
                {
                    var hash = hashes[currentId];
                    var val = hashToInt(hash, band);
                    var textsInBucket = new HashSet<int>();
                    if(buckets.ContainsKey(val))
                    {
                        textsInBucket = buckets[val];
                        foreach(var id in textsInBucket)
                        {
                            candidates[currentId].Add(id);
                            candidates[id].Add(currentId);
                        }
                    }
                    else
                    {
                        textsInBucket = new HashSet<int>();
                    }
                    textsInBucket.Add(currentId);
                    buckets[val] = textsInBucket;
                }
            }
            return candidates;
        }

        private static List<ISet<int>> createCandidatesMap(int count)
        {
            var rv = new List<ISet<int>>(count);
            for(var i = 0; i < count; i++)
            {
                rv.Add(new HashSet<int>());
            }
            return rv;
        }

        private static int hashToInt(byte[] hash, int band)
        {
            var rv = 0;
            rv |= hash[band * 2];
            rv <<= 8;
            rv |= hash[band * 2 + 1];
            return rv;
        }
    }
}
