﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAB01_B
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());

            var hashes = new List<byte[]>(n);
            for (var i = 0; i < n; i++)
            {
                hashes.Add(Simhash.Calculate(Console.ReadLine().Trim()));
            }
            var candidates = LSH.CalculateCandidates(hashes, 8);

            var q = int.Parse(Console.ReadLine());
            for (var j = 0; j < q; j++)
            {
                var parts = Console.ReadLine().Split(' ');
                var i = int.Parse(parts[0]);
                var k = int.Parse(parts[1]);

                var result = ExecuteQuery(candidates, hashes, i, k);
                Console.WriteLine(result);
            }
        }

        public static int ExecuteQuery(List<ISet<int>> candidates, List<byte[]> hashes, int i, int k)
        {
            var count = 0;
            var hash = hashes[i];
            foreach (var candidateId in candidates[i])
            {
                var distance = Simhash.HammingDistance(hash, hashes[candidateId]);
                if(distance <= k)
                {
                    ++count;
                }
            }
            return count;
        }
    }
}
