﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LAB01_B
{
    public class Simhash
    {
        public static byte[] Calculate(string text)
        {
            var sh = new int[128];
            foreach (var token in tokenize(text))
            {
                var hash = calculateMD5(token);
                for(var i = 0; i < hash.Length; i++)
                {
                    byte mask = 0x80;
                    for(var j = 0; j < 8; j++)
                    {
                        var index = i * 8 + j;
                        if((hash[i] & mask) != 0)
                        {
                            sh[index] += 1;
                        }
                        else
                        {
                            sh[index] -= 1;
                        }
                        mask >>= 1;
                    }
                }
            }

            var rv = new byte[16];
            for (var bitIdx = 0; bitIdx < sh.Length; bitIdx += 8)
            {
                var i = bitIdx / 8;
                byte mask = 0x80;
                for(var j = 0; j < 8; j++)
                {
                    if(sh[bitIdx + j] >= 0)
                    {
                        rv[i] |= mask;
                    }
                    mask >>= 1;
                }
            }

            return rv;
        }

        public static string ConvertToHex(byte[] bytes)
        {
            var sb = new StringBuilder(32);
            foreach(var b in bytes)
            {
                sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }

        public static int HammingDistance(byte[] lhs, byte[] rhs)
        {
            var rv = 0;
            for(var i = 0; i < lhs.Length; i++)
            {
                var diff = lhs[i] ^ rhs[i];
                for(var j = 0; j < 8; j++)
                {
                    if((diff & 0x01) == 0x01)
                    {
                        rv++;
                    }
                    diff >>= 1;
                }
            }
            return rv;
        }

        private static string[] tokenize(string text)
        {
            return text.Split(' ');
        }

        private static byte[] calculateMD5(string token)
        {
            using (var md5 = MD5.Create())
            {
                byte[] tokenBytes = Encoding.ASCII.GetBytes(token);

                return md5.ComputeHash(tokenBytes);
            }
        }
    }
}
