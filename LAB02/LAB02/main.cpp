#include <algorithm>
#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <unordered_map>

struct PCYData {
    int candidates;
    int pairs;
    std::vector<int> pair_counts;
};

struct Hash {
    size_t operator()(std::pair<int, int> const& p) const {
        size_t res = 17;
        res = res * 31 + std::hash<int>()(p.first);
        res = res * 31 + std::hash<int>()(p.second);
        return res;
    }
};

PCYData pcy(int basket_count, int bucketcount, int threshold, std::istream& stream);

int main() {

    auto baskets = 0;
    auto buckets = 0;
    auto s = 0.;
    std::cin >> baskets >> s >> buckets;

    auto res = pcy(baskets, buckets, static_cast<int>(std::floor(s * baskets)), std::cin);

    std::cout << res.candidates << '\n' << res.pairs << '\n';
    for (auto const& c : res.pair_counts) {
        std::cout << c << '\n';
    }
}

PCYData pcy(int basket_count, int bucket_count, int threshold, std::istream& stream) {
    auto baskets = std::vector<std::vector<int>>(basket_count);

    auto item_counts = std::unordered_map<int, int>();
    auto line = std::string();
    while (std::getline(stream, line)) {
        auto basket = std::vector<int>();

        std::istringstream bucket_stream{ line };
        auto item = 0;
        while (bucket_stream >> item) {
            ++item_counts[item];
            basket.push_back(item);
        }

        baskets.push_back(std::move(basket));
    }

    auto is_common_pair = [&ic = item_counts, t = threshold](int i, int j) {
        return ic[i] >= t && ic[j] >= t;
    };

    auto buckets = std::vector<int>(bucket_count, 0);
    for (auto const& basket : baskets) {
        for (auto i = basket.cbegin(); i < basket.cend(); ++i) {
            for (auto j = i + 1; j < basket.cend(); ++j) {
                if (is_common_pair(*i, *j)) {
                    auto k = (*i * item_counts.size() + *j) % bucket_count;
                    ++buckets[k];
                }
            }
        }
    }

    auto is_common_bucket = [&buckets, threshold](int b) {
        return buckets[b] >= threshold;
    };

    auto pairs = std::unordered_map<std::pair<int, int>, int, Hash>();
    for (auto const& basket : baskets) {
        for (auto i = basket.cbegin(); i < basket.cend(); ++i) {
            for (auto j = i + 1; j < basket.cend(); ++j) {
                if (is_common_pair(*i, *j)) {
                    auto k = (*i * item_counts.size() + *j) % bucket_count;
                    if (is_common_bucket(k)) {
                        ++pairs[{*i, *j}];
                    }
                }
            }
        }
    }

    auto rv = PCYData();

    auto m = 0;
    std::for_each(item_counts.cbegin(), item_counts.cend(), [&m, t = threshold](std::pair<int, int> const& p) {
        if (p.second >= t) {
            ++m;
        }
    });

    auto p = pairs.size();

    for (auto const& pair : pairs) {
        if (pair.second >= threshold) {
            rv.pair_counts.push_back(pair.second);
        }
    }
    std::sort(rv.pair_counts.rbegin(), rv.pair_counts.rend());

    rv.candidates = (m * (m - 1)) / 2;
    rv.pairs = p;

    return rv;
}