#include <algorithm>
#include <cstring>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

struct Bucket {
	int timestamp;
	int size;

	Bucket(int t, int s) : timestamp{ t }, size{ s } {}
};

int main() {
	size_t n;
	std::cin >> n;

	auto buckets = std::vector<Bucket>();

	std::string line;
	while (std::getline(std::cin, line)) {
		if (line[0] == 'q') {
			auto k = std::stoi(line.substr(2));

			auto it = std::lower_bound(buckets.begin(), buckets.end(), Bucket(k, 0), [](Bucket const& lhs, Bucket const& rhs) {
				return lhs.timestamp > rhs.timestamp;
			});

			auto approx = 0;
			if (it != buckets.end()) {
				approx = std::accumulate(it + 1, buckets.end(), it->size / 2, [](int const& a, Bucket const& b) {
					return a + b.size;
				});
			}

			std::cout << approx << '\n';
		}
		else {
			for (auto bit : line) {
				for (auto& bucket : buckets) {
					++bucket.timestamp;
				}
				if (!buckets.empty() && buckets[0].timestamp > n) {
					buckets.erase(buckets.begin());
				}

				if (bit == '1') {
					buckets.emplace_back(1, 1);

					auto buckets_to_merge = buckets.size() >= 3;
					auto check_from_index = buckets.size() - 1;
					while (buckets_to_merge) {
						buckets_to_merge = false;
						auto consecutive_count = 0;
						for (auto i = check_from_index + 1; i-- != 0;) {
							if (buckets[check_from_index].size == buckets[i].size) {
								++consecutive_count;
								if (consecutive_count == 3) {
									buckets_to_merge = true;
									break;
								}
							}
						}

						if (buckets_to_merge) {
							buckets.erase(buckets.begin() + check_from_index - 2);
							buckets[check_from_index - 2].size *= 2;
						}
						check_from_index -= 2;

						buckets_to_merge &= check_from_index >= 2;
					}
				}
			}
		}
	}
}