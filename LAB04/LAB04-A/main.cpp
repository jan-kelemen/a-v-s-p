#include <cstdio>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

struct Node {
	std::vector<int> neighbors;
};

std::vector<int> read_node_data(std::istream& stream) {
	std::string line;
	std::getline(stream, line);
	
	std::istringstream ss{ line };

	auto rv = std::vector<int>();
	rv.reserve(15);

	auto neighbor = 0;
	while (ss >> neighbor) {
		rv.push_back(neighbor);
	}

	return rv;
}

int main() {
	using iteration_t = std::vector<double>;

	auto n = 0;
	auto beta = .0;
	scanf("%d %lf\n", &n, &beta);

	auto nodes = std::vector<Node>(n);
	auto rank_cache = std::vector<iteration_t>();
	rank_cache.emplace_back(n, 1. / n);

	for (auto i = 0; i < n; ++i) {
		nodes[i].neighbors = read_node_data(std::cin);
	}

	auto q = 0;
	scanf("%d\n", &q);
	for (auto query = 0; query < q; ++query) {
		auto query_node = 0;
		auto iteration = 0;
		scanf("%d %d\n", &query_node, &iteration);

		if (iteration >= int(rank_cache.size())) {
			for (auto i = int(rank_cache.size()); i <= iteration; ++i) {
				rank_cache.emplace_back(n, (1 - beta) / n);
				for (size_t node_index = 0; node_index < nodes.size(); ++node_index) {
					auto& node = nodes[node_index];
					for (auto const& neighbor : node.neighbors) {
						rank_cache[i][neighbor] += beta * rank_cache[i - 1][node_index] / node.neighbors.size();
					}
				}
			}
		}

		auto rank = rank_cache[iteration][query_node];
		printf("%.10lf\n", rank);
	}
}