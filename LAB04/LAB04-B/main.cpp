#include <cstdio>
#include <set>
#include <vector>
#include <queue>

struct Node {
	bool is_black = false;
	std::vector<int> edges;
};

struct ClosestNode {

	ClosestNode(int n = -1, int d = -1) : node{ n }, distance{ d } {}

	int node;
	int distance;
};

bool operator<(ClosestNode const& lhs, ClosestNode const& rhs) {
	if (lhs.distance == -1) {
		return false;
	}
	if (rhs.distance == -1) {
		return true;
	}
	if (lhs.distance == rhs.distance) {
		return lhs.node < rhs.node;
	}
	return lhs.distance < rhs.distance;
}

bool operator==(ClosestNode const& lhs, ClosestNode const& rhs) {
	return lhs.node == rhs.node && lhs.distance == rhs.distance;
}

ClosestNode breadth_first_search(std::vector<Node> const& nodes, int root) {
	auto rv = ClosestNode();
	
	auto s = std::vector<bool>(nodes.size(), false);
	s[root] = true;
	auto current_level = std::queue<int>({ root });

	auto distance = 0;
	while (distance <= 10 && rv.distance == -1) {
		auto down = std::queue<int>();

		auto level_results = std::vector<ClosestNode>();
		while (!current_level.empty()) {
			auto current = current_level.front();
			current_level.pop();

			if (nodes[current].is_black) {
				level_results.emplace_back(current, distance);
			}

			for (auto const& n : nodes[current].edges) {
				if (!s[n]) {
					s[n] = true;
					down.push(n);
				}
			}
		}
		++distance;

		current_level = std::move(down);

		for (auto const& res : level_results) {
			if (res < rv) {
				rv = res;
			}
		}
	}

	return rv;
}

int main() {
	auto n = 0;
	auto e = 0;
	scanf("%d %d\n", &n, &e);

	auto nodes = std::vector<Node>(n);
	for (auto i = 0; i < n; ++i) {
		auto color = 0;
		scanf("%d\n", &color);
		if (color == 1) {
			nodes[i].is_black = true;
		}
	}
	for (auto i = 0; i < e; ++i) {
		auto first = 0;
		auto second = 0;
		scanf("%d %d\n", &first, &second);
		nodes[first].edges.push_back(second);
		nodes[second].edges.push_back(first);
	}

	for (auto i = 0; i < n; ++i) {
		auto data = breadth_first_search(nodes, i);
		printf("%d %d\n", data.node, data.distance);
	}
}