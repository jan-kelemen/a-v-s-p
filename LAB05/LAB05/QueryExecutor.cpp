#include "QueryExecutor.h"

#include <algorithm>
#include <numeric>

QueryExecutor::QueryExecutor(matrix_t m) : matrix{ std::move(m) } {
	subtract_row_means();
	calculate_magnitudes();
}

double QueryExecutor::execute(int const i, int const j, int const card) const {
	auto sim_index_vec = similarity_index(i, j, card);

	auto nom = 0.;
	auto denom = 0.;
	for(auto const& si : sim_index_vec) {
		denom += si.first;

		nom += si.first * matrix[si.second][j].first;
	}

	return nom / denom;
}

double QueryExecutor::cosine_similarity(int const first, int const second) const {
	auto const& first_row = matrix[first];
	auto const& second_row = matrix[second];

	auto sum = 0.;

	auto size = first_row.size();
	for (size_t i = 0; i < size; ++i) {
		sum += first_row[i].second * second_row[i].second;
	}

	return sum / (magnitudes[first] * magnitudes[second]);
}

std::vector<QueryExecutor::sim_index_t> QueryExecutor::similarity_index(int const row, int const col, int const card) const {
	auto rv = std::vector<sim_index_t>();

	for (size_t i = 0; i < matrix.size(); ++i) {
		if (matrix[i][col].first != 0) {
			auto const& sim = cosine_similarity(i, row);
			if (sim > 0) {
				rv.emplace_back(sim, i);
			}
		}
	}

	std::sort(rv.begin(), rv.end(), [](sim_index_t const& lhs, sim_index_t const& rhs) {
		return lhs.first> rhs.first;
	});

	rv.resize(std::min<size_t>(card, rv.size()));

	return rv;
}

void QueryExecutor::subtract_row_means() {
	auto rv = matrix_t(matrix.size());

	for (auto& row : matrix) {
		auto count = 0;
		auto sum = std::accumulate(row.cbegin(), row.cend(), 0, [&c = count](int curr, matrix_element_t const& v) {
			if (v.first != 0) {
				++c;
				return curr + v.first;
			}
			return curr;
		});
		auto mean = static_cast<double>(sum) / count;

		for (auto& elem : row) {
			if (elem.first != 0) {
				elem.second = elem.first - mean;
			}
		}
	}
}

void QueryExecutor::calculate_magnitudes() {
	for (auto& row : matrix) {
		auto squared_mag = std::accumulate(row.cbegin(), row.cend(), 0., [](double curr, matrix_element_t const& v) {
			return curr + v.second * v.second;
		});
		auto mag = std::sqrt(squared_mag);
		magnitudes.push_back(mag);
	}
}
