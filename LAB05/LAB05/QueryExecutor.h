#ifndef QUERY_EXECUTOR_H
#define QUERY_EXECUTOR_H

#include <utility>
#include <vector>

class QueryExecutor {
public:
	using matrix_element_t = std::pair<int, double>;
	using matrix_row_t = std::vector<matrix_element_t>;
	using matrix_t = std::vector<matrix_row_t>;

	QueryExecutor(matrix_t m);

	double execute(int const i, int const j, int const card) const;
protected:
	using magnitudes_t = std::vector<double>;
	using sim_index_t = std::pair<double, int>;

	double cosine_similarity(int const first, int const second) const;

	std::vector<sim_index_t> similarity_index(int const row, int const col, int const card) const;
	
	matrix_t matrix;
	magnitudes_t magnitudes;

private:
	void subtract_row_means();
	void calculate_magnitudes();
};

#endif // !QUERY_EXECUTOR_H
