#include "UserUserQueryExecutor.h"

UserUserQueryExecutor::UserUserQueryExecutor(matrix_t const& m) : QueryExecutor{ transpose(m) } {}

QueryExecutor::matrix_t UserUserQueryExecutor::transpose(QueryExecutor::matrix_t const& m) {
	int new_columns = m.size();
	int new_rows = m[0].size();

	auto rv = QueryExecutor::matrix_t(new_rows);
	for (auto& row : rv) {
		row.resize(new_columns);
	}

	for (auto i = 0; i < new_columns; ++i) {
		for (auto j = 0; j < new_rows; ++j) {
			rv[j][i] = m[i][j];
		}
	}

	return rv;
}
