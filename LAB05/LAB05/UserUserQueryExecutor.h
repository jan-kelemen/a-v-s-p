#ifndef USER_USER_QUERY_EXECUTOR
#define USER_USER_QUERY_EXECUTOR

#include "QueryExecutor.h"

class UserUserQueryExecutor : public QueryExecutor {
public:
	UserUserQueryExecutor(matrix_t const& m);

private:
	QueryExecutor::matrix_t transpose(QueryExecutor::matrix_t const& m);
};
#endif // !USER_USER_QUERY_EXECUTOR
