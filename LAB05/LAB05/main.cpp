#include <cstdio>
#include <iostream>

#include "ItemItemQueryExecutor.h"
#include "UserUserQueryExecutor.h"

QueryExecutor::matrix_t read_item_user_matrix(int const item_count, int const user_count) {
	auto rv = QueryExecutor::matrix_t(item_count);
	for (auto i = 0; i < item_count; ++i) {
		rv[i].reserve(item_count);

		for (auto j = 0; j < user_count; ++j) {
			char item;
			std::cin >> item;
			rv[i].emplace_back((item == 'X' ? 0 : item - '0'), 0.);
		}
	}

	return rv;
}


int main() {
	auto user_count = 0;
	auto item_count = 0;
	scanf("%d %d\n", &item_count, &user_count);

	auto item_user_matrix = read_item_user_matrix(item_count, user_count);

	auto item_exec = ItemItemQueryExecutor(item_user_matrix);
	auto user_exec = UserUserQueryExecutor(item_user_matrix);

	auto queries = 0;
	scanf("%d\n", &queries);

	for (auto i = 0; i < queries; ++i) {
		auto user = 0;
		auto item = 0;
		auto type = 0;
		auto card = 0;
		scanf("%d %d %d %d\n", &item, &user, &type, &card);
		--item;
		--user;

		double value;
		if (type == 0) {
			value = item_exec.execute(item, user, card);
		}
		else {
			value = user_exec.execute(user, item, card);
		}

		int first_five = static_cast<int>(value * 10000);
		if (first_five % 10 >= 5) {
			first_five += 10;
		}
		first_five /= 10;
		printf("%d.%03d\n", first_five / 1000, first_five % 1000);
	}
}